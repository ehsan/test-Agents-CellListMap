using LinearAlgebra, Random, Agents

mutable struct Obstacle
    pos::NTuple{2, Float64}
end

"""
    initialize_obstacles(n::Int32, extent, rng)

Return a random vector of positions of obstacles.
"""
function initialize_obstacles(n::Integer, extent, rng)
    rand_pos = [rand(rng, SVector{2, Float64}) .* extent[1] for _ in 1:n] 
    return rand_pos
end 

