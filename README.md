# test-Agents-CellListMap

Test CellListMap.jl and Agents.jl integration. This repository is a subset of a bigger repo.

# Running 

After activating the project, include `bacteria.jl` 

``` julia
include("bacteria.jl")
```

and define the model by

``` julia
model = initialize_model(n_rods=10, n_obs=50, d=1, l=2, seed=111, extent=(100.0, 100.0), dt=0.01, va=1.0, λ0=1e-4, k0=50.0)
```

then for running the simulation one can run either

``` julia
adf, _ = run!(model, agent_step!, model_step!, 10_000;
              agents_first = false,
              adata = [:pos, :θ, :image], showprogress=true)
```

or

``` julia
abmvideo("test.mp4", model, agent_step!, model_step!;
         am=rod_marker,
         framerate = 200, spf=20, frames = 1000,
         title = "Bacteria", static_preplot!,
         )
```

to make a video.

# Benchmarks of the new version:

Before output preallocation:

```julia
julia> @time bench();
 35.614818 seconds (2.06 G allocations: 52.718 GiB, 20.88% gc time)

julia> @time onlysteps();
 34.100943 seconds (2.06 G allocations: 52.545 GiB, 20.04% gc time)

```

After CellListMap upgrade to 0.7.20 (10% improvement, maybe)

```julia
julia> @time bench();
 31.105756 seconds (1.99 G allocations: 50.947 GiB, 23.06% gc time)

julia> @time onlysteps();
 30.788533 seconds (1.98 G allocations: 50.783 GiB, 24.89% gc time)

```

After reestructuring the code:

```julia
julia> @time bench();
  1.990303 seconds (3.32 M allocations: 250.238 MiB, 0.47% gc time)

julia> @time onlysteps();
  1.787281 seconds (231.58 k allocations: 32.594 MiB)

```
